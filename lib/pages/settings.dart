import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {

  bool _music = false;
  bool _sound = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
            appBar: AppBar(
              title: Text('Settings'),
              backgroundColor: Colors.lightGreen,
              centerTitle:true,
            ),
        body:
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                    color: Colors.amber[100],
                    padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 40.0),
                    height: 150.0,
                    width: 250.0,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          'Music',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),

                        SizedBox(
                          width: 30.0,
                        ),

                        Switch(value: _music,
                         onChanged: (value){
                           setState(() {
                             _music = value;
                           });
                         }),

                        /*Container(
                          child: IconButton(
                            icon: Icon(
                              Icons.music_note,
                              color: Colors.purple,
                            ), 
                            color: Colors.purple,
                            onPressed: () {
                              print('Music');
                            },
                          ),
                        ),*/
                        SizedBox(
                          height: 30.0,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                            child: Text(
                              'Sound',
                              style: TextStyle(
                                fontSize: 20.0,
                              ),
                            ),
                        ),
                        SizedBox(
                            width: 30.0,
                        ),
                        /*Container(
                          child: IconButton(
                            icon: Icon(
                              Icons.surround_sound,
                              color: Colors.purple,
                            ), 
                            color: Colors.purple,
                            onPressed: () {
                              print('Sound');
                            },
                          ),
                        ),*/
                        Switch(value: _sound,
                         onChanged: (value){
                           setState(() {
                             _sound = value;
                           });
                         }),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
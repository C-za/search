import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int count = 0;

  @override
  void initState() {
    print('init statement');
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    print('build statement');
    return SafeArea(
          child: Scaffold(
        body:
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[       
             Row(
               mainAxisAlignment: MainAxisAlignment.end,              
                children: <Widget>[
                  IconButton(
                    icon:Icon(
                      Icons.settings,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/settings');
                      print('settings');
                     },
                  ),
                ],
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              
              IconButton(
                icon:Icon(
                  Icons.play_arrow,
                ),
                 onPressed: () {
                   print('Play');
                   Navigator.pushNamed(context, '/quiz');
                 },),
            ],),
            RaisedButton(onPressed: (){
              setState((){
                count++;
              });
              },
              child: Text('counter is $count'),
            ),
          ],
        ),
      ),
    );
  }
}